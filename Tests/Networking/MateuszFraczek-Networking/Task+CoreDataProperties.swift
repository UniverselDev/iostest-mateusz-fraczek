//
//  Task+CoreDataProperties.swift
//  MateuszFraczek-Networking
//
//  Created by Mass on 29/01/2016.
//  Copyright © 2016 Mass. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Task {

    @NSManaged var done: NSNumber?
    @NSManaged var task: String?
    @NSManaged var image: String?

}
