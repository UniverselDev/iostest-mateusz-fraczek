//
//  ViewController.swift
//  MateuszFraczek-Networking
//
//  Created by Mass on 29/01/2016.
//  Copyright © 2016 Mass. All rights reserved.
//

import UIKit
import CoreData

let feedURL = "https://bitbucket.org/fatunicorn/iostest/raw/78f902ddb31ac96bb23b901054442dee52664271/data/tasklist.json"

class ViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
 
    // MARK: - Arrays using to display data in tableView
    var toDoList:[Task] = []
    var doneList:[Task] = []
    var allList:[Task] = []
    
    // Loading indicator used when downloading data
    var loadingIndicator: LoadingIndicator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        
        // Get items from Core Data
        requestItemsFromCoreData()
        
        // If Core Data is empty - make request
        if allList.count == 0 {
            makeRequest()
        }

    }

    func configureView() {
      
        // Show loading indicator
        loadingIndicator = LoadingIndicator(title: "Loading...", center: view.center)
        view.addSubview(loadingIndicator.getViewActivityIndicator())
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.hidden = true
    }
    

    @IBAction func segmentedControl(sender: UISegmentedControl) {
       self.tableView.reloadData()
    }
    
    enum JSONError: String, ErrorType {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }
    
    // Get data from URL
    func makeRequest() {
        guard let endpoint = NSURL(string: feedURL) else { print("Error creating endpoint");return }
        let request = NSMutableURLRequest(URL:endpoint)
        
        NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) -> Void in
            do {
                guard let dat = data else { throw JSONError.NoData }
                guard let json = try NSJSONSerialization.JSONObjectWithData(dat, options: []) as? NSArray else { throw JSONError.ConversionFailed }
                
                // Convert data from server to object Task and save to Core Data
                DataController.jsonToObject(json)
                
                // Request items from Core Data
                self.requestItemsFromCoreData()
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.tableView.reloadData()
                })
            } catch let error as JSONError {
                print(error.rawValue)
                
                // Show alert if error and try again or cancel
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let alert = UIAlertController(title: error.rawValue, message: "", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
                    alert.addAction(UIAlertAction(title: "Try again...", style: .Default, handler: { (alert) -> Void in
                        self.makeRequest()
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                })
                
            } catch {
                print(error)
            }
            }.resume()
    }
    
    // Get items from Core Data
    func requestItemsFromCoreData() {
        let fetchRequest = NSFetchRequest(entityName: "Task")
        let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let managedObjectContext = appDelegate.managedObjectContext
        
        do {
            self.allList = try managedObjectContext.executeFetchRequest(fetchRequest) as! [Task]
            
            // Add only uncompleted task
            self.toDoList = self.allList.filter( { $0.done == 0})
            
            // Add only completed task
            self.doneList = self.allList.filter( { $0.done != 0})
            
            self.loadingIndicator.stopAnimating()
            self.tableView.hidden = false
            
        } catch {
            print("error")
        }
        
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            return toDoList.count
        case 1:
            return doneList.count
        case 2:
            return allList.count
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")! as UITableViewCell
    
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            let thisItem = toDoList[indexPath.row]
            cell.textLabel?.text = thisItem.task
            return cell
        case 1:
            let thisItem = doneList[indexPath.row]
            cell.textLabel?.text = thisItem.task
            return cell
        case 2:
            let thisItem = allList[indexPath.row]
            cell.textLabel?.text = thisItem.task
            return cell
        default:
            return UITableViewCell()
        }
 
    }
}