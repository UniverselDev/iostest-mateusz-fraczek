//
//  DataController.swift
//  MateuszFraczek-Networking
//
//  Created by Mass on 29/01/2016.
//  Copyright © 2016 Mass. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DataController {
    
    class func jsonToObject(json: NSArray) {
        
        // JSON TREE
        for item in json {
            
            if item["done"] != nil {
                if item["image"] != nil {
                    if item["task"] != nil {
                        
                        let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
                        let entityDescription = NSEntityDescription.entityForName("Task", inManagedObjectContext: managedObjectContext)
                        
                        // Task Model
                        let task = Task(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
                        task.done = item["done"] as? Int
                        task.task = item["task"] as? String
                        task.image = item["image"] as? String
                        
                        (UIApplication.sharedApplication().delegate as! AppDelegate).saveContext()
                    }
                }
            }
        }
    }
}