//
//  ViewController.swift
//  MateuszFraczek-AV
//
//  Created by Mass on 28/01/2016.
//  Copyright © 2016 Mass. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    // AVFoundation Properties to capture session and preview
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    var captureDevice: AVCaptureDevice?
    var userImageView: UIImageView?
    
    // MARK: - IBOutlets
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var takePhotoButton: UIButton!
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var saveButtonPressed: UIButton!
    
    // Property to store image data
    var imageData: NSData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(animated: Bool) {
        
        // Configure navigation bar
        let navbarFont = UIFont(name: "AvenirNext-Regular", size: 20) ?? UIFont.systemFontOfSize(20)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: navbarFont]
        self.navigationController?.navigationBar.translucent = false
        
        configureCamera()
    }
    
    override func viewDidAppear(animated: Bool) {
        // Update preview frame
        previewLayer!.frame = previewView.bounds
    }
    
    // Main function to configure AVFoundation session and display video preview
    func configureCamera() {
        
        // Hide buttons
        self.tryAgainButton.hidden = true
        self.saveButtonPressed.hidden = true
        
        // Create Session
        captureSession = AVCaptureSession()
        captureSession!.sessionPreset = AVCaptureSessionPresetPhoto
        
        let devices = AVCaptureDevice.devices()
        // Loop through all the capture devices on this phone
        for device in devices {
            // Make sure this particular device supports video
            if (device.hasMediaType(AVMediaTypeVideo)) {
                // Finally check the position and confirm we've got the back camera
                if(device.position == AVCaptureDevicePosition.Front) {
                    captureDevice = device as? AVCaptureDevice
                    if captureDevice != nil {
                        
                        var error: NSError?
                        var input: AVCaptureDeviceInput!
                        do {
                            input = try AVCaptureDeviceInput(device: self.captureDevice)
                        } catch let error1 as NSError {
                            error = error1
                            input = nil
                        }
                        
                        if error == nil && captureSession!.canAddInput(input) {
                            captureSession!.addInput(input)
                            
                            stillImageOutput = AVCaptureStillImageOutput()
                            stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
                            if captureSession!.canAddOutput(stillImageOutput) {
                                captureSession!.addOutput(stillImageOutput)
                                
                                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                                previewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
                                previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.Portrait
                                previewView.layer.addSublayer(previewLayer!)
                                
                                captureSession!.startRunning()
                            }
                        }

                    }
                }
            }
        }
    }

    // MARK: - IBActions
    @IBAction func takePhotoPressed(sender: UIButton) {
        
        if let videoConnection = stillImageOutput?.connectionWithMediaType(AVMediaTypeVideo) {
            videoConnection.videoOrientation = AVCaptureVideoOrientation.Portrait
            stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: {(sampleBuffer, error) in
                if (sampleBuffer != nil) {
                    
                    // Convert image data to image
                    self.imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                    let dataProvider = CGDataProviderCreateWithCFData(self.imageData)
                    let cgImageRef = CGImageCreateWithJPEGDataProvider(dataProvider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)
                    
                    let image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.LeftMirrored)
                    
                    // Show image preview
                    self.userImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.previewView.frame.size.width, height: self.previewView.frame.size.height))
                    self.userImageView!.image = image
                    self.previewView.addSubview(self.userImageView!)
                    
                    // Hide & Show buttons
                    self.takePhotoButton.hidden = true
                    self.tryAgainButton.hidden = false
                    self.saveButtonPressed.hidden = false
                }
            })
        }

    }

    @IBAction func saveImageButtonPressed(sender: UIButton) {
        
        // Convert image data to image
        let dataProvider = CGDataProviderCreateWithCFData(self.imageData)
        let cgImageRef = CGImageCreateWithJPEGDataProvider(dataProvider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)
        
        let image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.RightMirrored)
        
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        
        // Show alert after saved
        let alert = UIAlertController(title: "Selfie Saved", message: "", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Take next", style: .Cancel, handler: { (alert) -> Void in
            self.captureSession!.startRunning()
            self.userImageView?.removeFromSuperview()
            
            self.takePhotoButton.hidden = false
            self.tryAgainButton.hidden = true
            self.saveButtonPressed.hidden = true
        }))
        self.presentViewController(alert, animated: true, completion: nil)

    }

    @IBAction func tryAgainButtonPressed(sender: UIButton) {
        
        // Run AV session again
        captureSession!.startRunning()
        self.userImageView?.removeFromSuperview()
        self.takePhotoButton.hidden = false
        self.tryAgainButton.hidden = true
        self.saveButtonPressed.hidden = true
    }
    
    
}

