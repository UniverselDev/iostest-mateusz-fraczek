//
//  README.swift
//  MateuszFraczek-Autolayout
//
//  Created by Mass on 28/01/2016.
//  Copyright © 2016 Mass. All rights reserved.
//

/*
I think solution for this task is the best when "email textField" is pinned to Top Layout. In this case we can use email/password textFields and button on iPhones (even 4s) and iPads.

In this task you want to pin UI elements in center view. The best solution is to use notifications and get keyboard size.

1. Add observer with funtions keyboardWillShow and keyboardWillHide

NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)

2.

func keyboardWillShow(notification: NSNotification) {
    if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()
        // Update constraint here and use "keyboardSize.height"
    }

}

func keyboardWillHide(notification: NSNotification) {
    // Update constrant to previous values
}

3. Remove bbserver

deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
}


I think this is better solution because many people still use iPhone 4s. 

*/